jQuery(document).ready(function($) {

  // Removing the error block on search form.
  $('#clientsidevalidation-views-exposed-form-search-results-page-errors').remove();

  $('.page-collection .visuels_collection').bxSlider({
    mode: "fade"
  });

  $('.bxslider').bxSlider({
    mode: "fade",
    onSliderLoad: function (currentIndex) {
      $('.bx-wrapper').find('.bx-viewport').find('ul').children().eq(currentIndex).addClass('active-slide');

      if($('.bx-pager .bx-pager-item').length<=1)
        $('.bx-pager').remove();

    },
    onSlideBefore: function ($slideElement) {
      $('.bx-wrapper').find('.bx-viewport').find('ul').children().removeClass('active-slide');
      $slideElement.addClass('active-slide');
    }
  });

  $('.bxslider-landing').bxSlider({
    mode: "horizontal",
    slideWidth: 475,
    slideMargin: 30,
    pager: false,
    minSlides: 1,
    maxSlides: 2,
    onSliderLoad: function (currentIndex) {
      $('.bx-wrapper').find('.bx-viewport').find('ul').children().eq(currentIndex).addClass('active-slide');
    },
    onSlideBefore: function ($slideElement) {
      $('.bx-wrapper').find('.bx-viewport').find('ul').children().removeClass('active-slide');
      $slideElement.addClass('active-slide');
    }
  });

  var panzoom_product;
  $(".link-enlarge").click(function () {
    var picture = $(".bxslider").find('li.active-slide img');
    var fid = picture.data("fid");
    var type = picture.data("type");
    $(".product-zoom").addClass(picture.attr('data-type') + "_container").fadeIn();
    var request = $.ajax({
      url: "/getmypicture/" + fid,
      type: "GET",
      dataType: "text"
    });
    request.done(function (reponse) {
      var html = '<img id="product-zoom-elevated" src="' + reponse + '" class="adaptive-image ' + type + '" />';
      $("#product-zoom-picture").removeClass('loading').append(html);
    });
    request.fail(function (request, textStatus) {
      alert("Unable to load the picture " + textStatus);
    });


    if($('body').hasClass('mobile'))
    {
      panzoom_product = $(".product-zoom #product-zoom-picture").panzoom({
        minScale: 1,
      });
      panzoom_product.panzoom('enable');
      $(window).disablescroll();
    }


  });
  $(".zoom-close").click(function () {
    $(".product-zoom").fadeOut();
    $("#product-zoom-picture").addClass('loading').empty();
    $(window).disablescroll("undo");
    panzoom_product.panzoom('reset');
    panzoom_product.panzoom('disable');
  });

  /** product video */
  $('.button-video').click(function () {
    $('.productvideo').toggle();
    $('.bx-wrapper').toggle();
  });

  /** product sharer */
  $('.product-action-item-share').on('click', function () {
    $(this).addClass('is-active');
  });
  $('body').on('click', function (event) {
    if ($('.product-action-item-share').hasClass('is-active')) {
      var result = $(event.target).closest('#tooltip-share').length || $(event.target).closest('.product-action-item-share').length;

      if (result === 0) {
        $('.product-action-item-share').removeClass('is-active');
      }
    }
  });

  $(".popin").fancybox();

  /** point of sale */
  $('.form-item-region .select-items').click(function () {
  });

  $('.onglets li').on('click',function(){
    if(!$(this).hasClass('active')){
      $('.onglet').toggleClass('show');
      $('.onglets li').removeClass('active');
      $(this).addClass('active');
    }
    return false;
  });


  if ($('body').hasClass('page-point-of-sale-locations') && (!$('body').hasClass('mobile') || $(window).width()<760)) {
    // Calculate the total height.
    var totalHeight = 0;
    var minHeight = 300;
    $('.search-result-item').each(function() {
      totalHeight += $(this).outerHeight();
    });
    if (totalHeight > minHeight) {

      $(window).bind("load", function() {
        var scroll = $('.SearchResult').tinyscrollbar();

        $(".page-point-of-sale-locations .SearchResult .thumb").bind( "movestart", function( event, ui ){
          /* do something here */
          console.log("ok");
        });
      });
    }
    else {
      $('div.scrollbar').hide();
      $('.SearchResult').css('margin-right', '20px');
    }
    $('.gmap-map').height($('.locator-col-left').height()-153);
    $(window).resize(function(){
      $('.gmap-map').height($('.locator-col-left').height()-153);
    });
  }

  $('body').on('click', function (event) {
    if ($(event.target).closest('.select-placeholder').length < 1) {
      $('.selectMenu').hide();
      $('.form-item-active').removeClass('form-item-active');
    }
  });
  if($('body').hasClass('page-watch-finder')){
    $selected_items = new Array();
    $('.page-watch-finder .col1>.form-item>label,.page-watch-finder .col2>.form-item>label,.page-watch-finder .col3>.form-item>label').on('click', function (event) {
      $name = $(this).parent('.form-item').attr('class');
      $name = $name.replace("form-item form-type-checkboxes ", "");

      if($.inArray($name,$selected_items)=='-1')
        $selected_items.push($name);
      else
        $selected_items.splice($.inArray($name,$selected_items), 1);

      $(this).parent().find('.form-checkboxes').toggleClass('active');
    });

    $(document).ajaxComplete(function() {

      $.each($selected_items, function(key, val) {
        $('.'+val+' .form-checkboxes').addClass('active');
      });


      $('.page-watch-finder .col1>.form-item>label,.page-watch-finder .col2>.form-item>label,.page-watch-finder .col3>.form-item>label').on('click', function (event) {
        $name = $(this).parent('.form-item').attr('class');
        $name = $name.replace("form-item form-type-checkboxes ", "");

        if($.inArray($name,$selected_items)=='-1')
          $selected_items.push($name);
        else
          $selected_items.splice($.inArray($name,$selected_items), 1);

        $(this).parent().find('.form-checkboxes').toggleClass('active');
      });

    });
  }

  $(".menu-items-list.content").mouseenter(function () {
    $this = $(this);
    var img_url = $this.find('a').attr('data-img');
    var parent = $this.closest("div.submenu");

    var img = $("<img />").attr('src', img_url).load(function(){
      $(parent).css('background-image', 'url("' + img_url + '")');
      $(this).remove();
    });

  });

  var fid;
  var current_fid = 0;
  $(".menu-items-list.collection").mouseenter(function () {
    $this = $(this);
    var parent = $this.closest("div.submenu");
    var attr_img = $this.find('a').attr('data-img');
    parent.find(".picture_area").addClass('loading').html('');
    var img = $("<img />").attr('class','adaptive-image').attr('src', attr_img).load(function(){
      parent.find(".picture_area").removeClass('loading').html(img);
    });

  });

  $(".main-menu-enhanced li").each(function(){
    $(this).find(".submenu .item-list:first .menu-items-list.collection:first").trigger('mouseenter');
  }).hover(function(){
    //$(this).find("div.submenu").css('background-image','');
  },function(){
    $(this).find("div.submenu").css('background-image','');
  });

  $("body.page-faq .view-content .views-row").click(function() {
    $(this).toggleClass("active").find('span').toggle("slow");
  });

  // PAGE PRODUIT
  $('.more-desc').click(function(){
    $(this).toggleClass('active');
    $('.col-right .description .field-name-field-product-description,.col-right .description .field-name-field-product-description').toggleClass('open');
    return false;
  })

  /** art */
  if( $('.blueframe').size()>0){
    $('.blueframe').onScreen({
      container: window,
      direction: 'vertical',
      doIn: function() {
        $(".blueframe").html('<iframe src="/sites/all/themes/harryw/artofwatch/bleue/bleu.html" width="641px" height="515px"/>');
      },
      doOut: function() {
        $(".blueframe").html();
      },
      tolerance: 300,
      throttle: 50,
      toggleClass: 'onScreen',
      lazyAttr: null,
      debug: false
    });
  }
  if( $('.joyauxframe').size()>0){
    $('.joyauxframe').onScreen({
      container: window,
      direction: 'vertical',
      doIn: function() {
        $(".joyauxframe").html('<iframe src="/sites/all/themes/harryw/artofwatch/pince/pince.html" width="344px" height="338px" />');
      },
      doOut: function() {
        $(".joyauxframe").html();
      },
      tolerance: 300,
      throttle: 50,
      toggleClass: 'onScreen',
      lazyAttr: null,
      debug: false
    });
  }
  if( $('.aquarelframe').size()>0){
    $('.aquarelframe').onScreen({
      container: window,
      direction: 'vertical',
      doIn: function() {
        $(".aquarelframe").html('<iframe src="/sites/all/themes/harryw/artofwatch/petales/petales.html" width="491px" height="425px" />');
      },
      doOut: function() {
        $(".aquarelframe").html();
      },
      tolerance: 300,
      throttle: 50,
      toggleClass: 'onScreen',
      lazyAttr: null,
      debug: false
    });
  }
  if( $('.plumeframe').size()>0){
    $('.plumeframe').onScreen({
      container: window,
      direction: 'vertical',
      doIn: function() {
        $(".plumeframe").html('<iframe src="/sites/all/themes/harryw/artofwatch/plume/plumes.html" width="535px" height="515px" />');
      },
      doOut: function() {
        $(".plumeframe").html();
      },
      tolerance: 300,
      throttle: 50,
      toggleClass: 'onScreen',
      lazyAttr: null,
      debug: false
    });
  }
  if( $('.movframe').size()>0){
    $('.movframe').onScreen({
      container: window,
      direction: 'vertical',
      doIn: function() {
        $(".movframe").html('<iframe src="/sites/all/themes/harryw/artofwatch/opus/opus.html" width="400px" height="480px" />');
      },
      doOut: function() {
        $(".movframe").html();
      },
      tolerance: 300,
      throttle: 50,
      toggleClass: 'onScreen',
      lazyAttr: null,
      debug: false
    });
  }
  if( $('.handframe').size()>0){
    $('.handframe').onScreen({
      container: window,
      direction: 'vertical',
      doIn: function() {
        $(".handframe").html('<iframe src="/sites/all/themes/harryw/artofwatch/main/main_bijoux.html" width="611px" height="538px" />');
      },
      doOut: function() {
        $(".handframe").html();
      },
      tolerance: 300,
      throttle: 50,
      toggleClass: 'onScreen',
      lazyAttr: null,
      debug: false
    });
  }

  if($('#no-doublearrow').size()>=1){
    $('#main-wrapper hr.arrow-bottom').hide();
  }

  /** Adapting and adjustments */
  // Removing 4 columns grid class if 3 or less items.
  var nGridItems = $('ul.grid-items .views-row').length;
  if (nGridItems < 4) {
    $('ul.grid-items').removeClass('grid-4-cols');
    $('div.view-content').css('margin','auto');
  }

  // Product suggestion: removing <hr> if no suggestion.
  if (!$('.productSuggests ul.grid-items .grid-item').length) {
    $('hr.suggests-separator').remove();
  }

  // Removing the "more" link when uneeded.
  if ($('.description .field-name-field-product-description .field-item').height() < 115){
    $('.node-type-jewelry-products #main-wrapper .productMain .col-right .description .field-name-field-product-description, .node-type-timepieces #main-wrapper .productMain .col-right .description .field-name-field-product-description').removeClass('long_text');
    $('.description a.more-desc').hide();
  }
  else{
    $('.description a.more-desc').show();
    $('.node-type-jewelry-products #main-wrapper .productMain .col-right .description .field-name-field-product-description, .node-type-timepieces #main-wrapper .productMain .col-right .description .field-name-field-product-description').addClass('long_text');
  }

  $(window).resize(function(){
    if ($('.description .field-name-field-product-description .field-item').height() < 115){
      $('.node-type-jewelry-products #main-wrapper .productMain .col-right .description .field-name-field-product-description, .node-type-timepieces #main-wrapper .productMain .col-right .description .field-name-field-product-description').removeClass('long_text');
      $('.description a.more-desc').hide();
    }
    else{
      $('.description a.more-desc').show();
      $('.node-type-jewelry-products #main-wrapper .productMain .col-right .description .field-name-field-product-description, .node-type-timepieces #main-wrapper .productMain .col-right .description .field-name-field-product-description').addClass('long_text');
    }
  });

  // Adjusting the technical specifications columns.
  if (!$('body').hasClass('mobile')) {
    var lileft = $('.productspecs .col-left').find('li');
    var liright = $('.productspecs .col-right').find('li');
    var n = lileft.length - liright.length;
    var m = parseInt(n/2);
    if (n > 1)
      lileft.slice(-m).prependTo('.productspecs .col-right ul');
    else if (n < 0)
      liright.slice(0,-n-m).appendTo('.productspecs .col-left ul');
  }

  /** News */
  if($('body').hasClass('page-news-events') || $('body').hasClass('node-type-news')){

    $('.slider_img.slider_multiple').bxSlider();

    var list_margin = 60;

    if($('body').hasClass('page-news-events'))
      list_margin =140

    $(document).scroll(function(){
      if($(document).scrollTop()>=$('.header').outerHeight()+list_margin)
      {
        if(($(document).scrollTop()+$(window).height())<=($(document).height()-$('.footer').height()))
          $('#content #block-views-news-events-block-1').css('top',$(document).scrollTop()-$('.header').outerHeight()-list_margin);
      }
      else{
        $('#content #block-views-news-events-block-1').css('top',0);
      }
    });
  }
});
