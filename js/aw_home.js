jQuery(document).ready(function($){
  $('.view-homepage-slider ul').bxSlider({
    mode:"fade",
    auto:true,
    pause:6000,
    nextText:"Next",
    prevText:"Back"
  }).find('li').height($(window).height());
  $('.bx-viewport').height($(window).height());

  $(window).resize(function(){
    $('.view-homepage-slider ul li').height($(window).height());
    $('.bx-viewport').height($(window).height());
  });

  // Welcome page.
  $(window).load(function() {
    $("#welcome_overlay").delay(5000).fadeOut('slow');
  });
  $("#welcome_overlay").click(function() {
      $(this).stop().fadeOut('slow');
  });
  if ($('.mobile').length > 0 || $('.tablet').length > 0) {
    $("#welcome_overlay").hide();
  }
});
