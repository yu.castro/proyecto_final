 jQuery(document).ready(function($){
    init();
    window.addEventListener("resize", function(){
    	init();
    },false);
    window.addEventListener("orientationchange", function(){
		init();
    },false);
    
     // Général
     $('a.scrollto').click(function(){
        if($(this).attr('href')!='')
            $('html, body').animate( { scrollTop: $($(this).attr('href')).offset().top },750);
        return false;   
     });
     
     $('.no-clickable').click(function(){
        return false; 
     });
 
     //Header
        //Searchbox
         $('.header .region-header .views-widget-filter-search_api_views_fulltext label').click(function(){
            $(".views-widget-filter-search_api_views_fulltext").toggleClass('is-search-active');
            $(".header .region-header .views-submit-button").toggleClass('is-search-active');
	    $(".mobile .header .region-header").toggleClass('active_mobile')
        });
        $('.header .region-header .views-widget-filter-search_api_views_fulltext .views-widget input[type=text]').val('');
	
	//Searchbox mobile
	$('#search-toggle').click(function(){
		$('.header .region-header .views-widget-filter-search_api_views_fulltext label').trigger('click');
	});
         
        //Menu mobile
         $('.menu-burger').click(function(){
           $(this).toggleClass('menu-burger--is-active');
           $('.mobile .header .region-main-menu').stop().fadeToggle();
        });
    
     //Footer
     $('.logo-footer a').click(function(){
         return false;
     });

   // FOOTER TABS.

   // Error in newsletter tab.
   //if ($('#block-harrywsnews-harrys-newsletter').find('.error').filter(':visible')) {
   //  $('.footer').addClass('active');
   //  $('#block-harrywsnews-harrys-newsletter').show();
   //}
   //else {
     // Opening/closing tabs.
     $('li.btn-footer-submenu').on('click', function(e) {
       e.preventDefault();
       $('.footer').addClass('active');
         if($('.btn-footer-submenu.submenu-is_active').size()>0){     //Si un menu est ouvert
           if($(this).hasClass('submenu-is_active')){
             $('.footer_submenu').slideUp();
             $('.footer').removeClass('active');
             $('li.btn-footer-submenu').removeClass('submenu-is_active');
           }else{
             var itemMenu = $(this);
             $('.footer_submenu.isOpen').slideUp(400,function(){
               $(this).removeClass('isOpen');
               $('li.btn-footer-submenu').removeClass('submenu-is_active');
               itemMenu.addClass('submenu-is_active');
               $('.'+itemMenu.attr('id')).slideDown().addClass('isOpen');
             });
           }
         }else{                                                  //Si les menus ont fermé
           $(this).addClass('submenu-is_active');
           $('.'+$(this).attr('id')).slideDown().addClass('isOpen');
         }
       
     });

     // Close the tab.
     $('.footer .close').on('click', function(e) {
       e.preventDefault();
       $('.footer').removeClass('active');
       $('.footer_submenu').slideUp().removeClass('isOpen');
       $('li.btn-footer-submenu').removeClass('submenu-is_active');
     });
   //}

    // FUNCTIONS
    
    function init(sim){
        $('meta[name="viewport"]').attr('content','width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0');
        var win_width = $(window).width();
        if((win_width<=768 && !$('body').hasClass('real_tablet')) || $('body').hasClass('real_mobile'))
        {
            $('body').addClass('mobile').removeClass('desktop').removeClass('tablet');
            $('meta[name="viewport"]').attr('content','width=750, user-scalable=0');
        }else if((win_width<1080 && win_width>768)  || $('body').hasClass('real_tablet')){

           if(window.orientation==0 || window.orientation==180){
	   	$('body').addClass('mobile').removeClass('desktop').removeClass('tablet');
                $('meta[name="viewport"]').attr('content','width=750, initial-scale=1, maximum-scale=1');
           }else{
	   	$('body').addClass('tablet').removeClass('desktop').removeClass('mobile');
                $('meta[name="viewport"]').attr('content','width=1024, initial-scale=1, maximum-scale=1');
           }
		//alert($('meta[name="viewport"]').attr('content'));
	   }else{
            $('body').addClass('desktop').removeClass('tablet').removeClass('mobile');
            $('.header .region-main-menu').fadeIn();
            $('meta[name="viewport"]').attr('content','width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0');
        }  
        
        if(win_width<1300)
            $('body').addClass('midScreen');
        else
            $('body').removeClass('midScreen');
	    
	if(win_width<980)
            $('body').addClass('smallScreen');
        else
            $('body').removeClass('smallScreen');
        
        //alert($('meta[name="viewport"]').attr('content'));
    }
    
 });